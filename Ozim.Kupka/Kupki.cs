﻿namespace Ozim.Kupka
{
    public class Kupki
    {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public bool Kupa { get; set; }
        public bool Siku { get; set; }
        public string? Comment { get; set; }
    }
}
