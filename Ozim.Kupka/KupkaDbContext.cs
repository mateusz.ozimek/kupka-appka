﻿using Microsoft.EntityFrameworkCore;

namespace Ozim.Kupka
{
    public partial class KupkaDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public KupkaDbContext(DbContextOptions<KupkaDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Kupki> Kupki { get; set; }
    }
}
