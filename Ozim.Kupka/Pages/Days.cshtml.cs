﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace Ozim.Kupka.Pages
{
    public class DaysModel : PageModel
    {
        [BindProperty]
        public DateTime datetime { get; set; }
        [BindProperty]
        public bool kupa { get; set; }
        [BindProperty]
        public bool siku { get; set; }
        [BindProperty]
        public bool karma_100g { get; set; }
        [BindProperty]
        public bool karma_200g { get; set; }
        [BindProperty]
        public string? comment { get; set; }

        private readonly ILogger<DaysModel> _logger;
        private readonly KupkaDbContext _ktx;
        public Dictionary<DateTime, List<DayItem>> items = new Dictionary<DateTime, List<DayItem>>();

        public DaysModel(ILogger<DaysModel> logger, KupkaDbContext ktx)
        {
            _logger = logger;
            _ktx = ktx;
        }

        public void OnGet()
        {
            items = _ktx.Kupki.Where(x => x.CreateDate > DateTime.Now.AddDays(-7).Date).OrderByDescending(x => x.CreateDate).ToList()
                .GroupBy(s => s.CreateDate.Date)
                .ToDictionary(gdc => gdc.Key, gdc => gdc.Select(s => new DayItem
                {
                    Id = s.Id,
                    datetime = s.CreateDate,
                    date = s.CreateDate.Date,
                    kupa = s.Kupa,
                    siku = s.Siku,
                    comment = s.Comment
                }).ToList());
        }

        public void OnPost()
        {
            bool nope = kupa;
            bool sik = siku;
            var tt = datetime;
            bool kk_100g = karma_100g;
            bool kk_200g = karma_200g;

            if (kk_100g) comment = "karma 100g\r\n" + comment;
            if (kk_200g) comment = "karma 200g\r\n" + comment;

            _ktx.Kupki.Add(new Kupki()
            {
                CreateDate = tt,
                Kupa = nope,
                Siku = sik,
                Comment = comment
            });
            _ktx.SaveChanges();
            items = _ktx.Kupki.Where(x => x.CreateDate > DateTime.Now.AddDays(-7).Date).OrderByDescending(x => x.CreateDate).ToList()
                .GroupBy(s => s.CreateDate.Date)
                .ToDictionary(gdc => gdc.Key, gdc => gdc.Select(s => new DayItem
                {
                    Id = s.Id,
                    datetime = s.CreateDate,
                    date = s.CreateDate.Date,
                    kupa = s.Kupa,
                    siku = s.Siku,
                    comment = s.Comment
                }).ToList());
        }
    }

    public class DayItem
    {
        public int Id { get; set; }
        public DateTime datetime { get; set; }
        public DateTime date { get; set; }
        public bool kupa { get; set; }
        public bool siku { get; set; }
        public string comment { get; set; }
    }
}