﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace Ozim.Kupka.Pages
{
    public class IndexModel : PageModel
    {
        [BindProperty]
        public DateTime datetime { get; set; }
        [BindProperty]
        public bool kupa { get; set; }
        [BindProperty]
        public bool siku { get; set; }
        [BindProperty]
        public string? comment { get; set; }

        private readonly ILogger<IndexModel> _logger;
        private readonly KupkaDbContext _ktx;
        public List<TableItem> items = new List<TableItem>();

        public IndexModel(ILogger<IndexModel> logger, KupkaDbContext ktx)
        {
            _logger = logger;
            _ktx = ktx;
        }

        public IActionResult OnGet()
        {
            return Redirect("/Days");
           /* items = _ktx.Kupki.OrderByDescending(x => x.CreateDate).Select(s => new TableItem
            {
                Id = s.Id,
                datetime = s.CreateDate,
                kupa = s.Kupa,
                siku = s.Siku,
                comment = s.Comment
            }).ToList();*/
        }

        public void OnPost()
        {
            bool nope = kupa;
            bool sik = siku;
            var tt = datetime;

            _ktx.Kupki.Add(new Kupki()
            {
                CreateDate = tt,
                Kupa = nope,
                Siku = sik,
                Comment = comment
            });
            _ktx.SaveChanges();
            items = _ktx.Kupki.OrderByDescending(x => x.CreateDate).Select(s => new TableItem
            {
                Id = s.Id,
                datetime = s.CreateDate,
                kupa = s.Kupa,
                siku = s.Siku,
                comment = s.Comment
            }).ToList();
        }
    }

    public class TableItem {
        public int Id { get; set; }
        public DateTime datetime { get; set; }
        public bool kupa { get; set; }
        public bool siku { get; set; }
        public string comment { get; set; }
    }
}