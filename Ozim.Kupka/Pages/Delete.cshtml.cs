using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Ozim.Kupka.Pages
{
    public class DeleteModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly KupkaDbContext _ktx;

        public DeleteModel(ILogger<IndexModel> logger, KupkaDbContext ktx)
        {
            _logger = logger;
            _ktx = ktx;
        }

        [BindProperty(Name = "Id",SupportsGet = true)]
        public string Id { get; set; }
        public void OnGet()
        {
        }
        public IActionResult OnPost()
        {
            Kupki kk = new Kupki() { Id = Int32.Parse(Id) };
            _ktx.Kupki.Remove(kk);
            _ktx.SaveChanges();

            return Redirect("/");
        }
    }
}
