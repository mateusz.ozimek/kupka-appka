using Ozim.Kupka;
using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddDbContext<KupkaDbContext>(
    options => options.UseMySql(@"server=192.168.0.17;Port=3307;uid=czesiek;password=vz0d4UgDOY7PE!;database=kupka", ServerVersion.AutoDetect(@"server=192.168.0.17;Port=3307;uid=czesiek;password=vz0d4UgDOY7PE!;database=kupka"))
);
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
}
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

app.Run();
